const key = 'a8814bee6d62eac3c380bfd0a8fd8d14';
const token = 'c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e';

const boardID = '5c946deb516ab08aad160e8f';
const listsID = '5c946deb516ab08aad160e90';
const cardID = '5c94797475f576612561ca1c';
let checkItems = {};

(function getChecklist() {
  fetch(`https://api.trello.com/1/cards/${cardID}/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=${key}&token=${token}`)
    .then(res => res.json())
    .then(data => {
      // console.log(data);
      const addElement = document.querySelector('.checklist-form');

      for (let i = 0; i < data.length; i++) {
        const div = document.createElement('div');
        div.className = 'checklist';
        div.id = data[i]['id'];
        const btn = document.createElement('button');
        const additem = document.createElement('button');
        btn.addEventListener('click', function () {
          deleteChecklist(data[i]['id']);
          // console.log(data[i]['id'])
        })
        additem.addEventListener('click', function () {
          addItem(data[i]['id']);
          // console.log(data[i]['id'])
        })
        const p = document.createElement('p');

        div.appendChild(p).innerHTML = data[i]['name'];
        div.appendChild(additem).innerHTML = 'Add item';
        div.appendChild(btn).innerHTML = 'delete';

        addElement.appendChild(div);
        // console.log(data)
        const newDiv = document.createElement('div');
        newDiv.className = 'checklist-item';
        newDiv.id = data[i]['id'];
        addElement.appendChild(newDiv);
        getChecklistItem(newDiv, data[i]['id']);
        // console.log(data[i]['id']);

      }
    })
})();

function getChecklistItem(myNode, checklistID) {
  // console.log(checklistID);
  // let checklistItem = [];

  fetch(`https://api.trello.com/1/checklists/${checklistID}/checkItems?filter=all&fields=name%2C%20nameData%2C%20pos%2C%20state&key=${key}&token=${token}`)
    .then(data => data.json())
    .then(jsonData => {
      for (let i of jsonData) {
        // console.log(i);
        const btn = document.createElement('input');
        btn.setAttribute('type', 'checkbox');
        btn.setAttribute('class', 'checklist-item-btn')

        fetch(`https://api.trello.com/1/cards/${cardID}/checkItem/${i['id']}?fields=name%2CnameData%2Cpos%2Cstate&key=${key}&token=${token}`)
          .then(itemdata => itemdata.json())
          .then(jitem => {
            btn.value = jitem['state'];
            // console.log(jitem)
            if (jitem['state'] == 'complete') {
              btn.setAttribute('checked', 'false');
            }
          });

        const dtl = document.createElement('input');
        dtl.setAttribute('type', 'button');
        dtl.setAttribute('class', 'checklist-item-dtl');
        dtl.setAttribute('value', 'cancel');

        const small = document.createElement('small');
        small.setAttribute('class', 'checklist-item-input');
        small.innerHTML = i['name'];

        const TempDiv = document.createElement('div');
        TempDiv.setAttribute('class', 'tempDIV');
        TempDiv.appendChild(small);
        TempDiv.appendChild(btn);
        TempDiv.appendChild(dtl);
        TempDiv.setAttribute('id', i['id']);
        myNode.appendChild(TempDiv);
        // console.log(TempDiv)
        // console.log(i);
        btn.addEventListener('click', function () {
          checkrUncheck(i['idChecklist'], i['id']);
        })
        dtl.addEventListener('click', function () {
          removeChecklistItem(i['idChecklist'], i['id']);
          // console.log(TempDiv)
        })
      }
    })
}

function checkrUncheck(checklistID, itemID) {
  const valueOfElement = document.getElementById(itemID).childNodes;
  // console.log(valueOfElement[1].value == 'complete');
  // console.log(valueOfElement[1]);
  // console.log(itemID);
  // console.log(checklistID);
  if (valueOfElement[1].value == 'complete') {
    fetch(`https://api.trello.com/1/cards/${cardID}/checkItem/${itemID}?state=incomplete&idChecklist=${checklistID}&key=${key}&token=${token}`, {
      method: 'PUT',
    }).then(data => data.json())
      .then(jdata => console.log(jdata));
    valueOfElement[1].value = 'incomplete';
  }
  else {
    fetch(`https://api.trello.com/1/cards/${cardID}/checkItem/${itemID}?state=complete&idChecklist=${checklistID}&key=${key}&token=${token}`, {
      method: 'PUT',
    })
    valueOfElement[1].value = 'complete';
  }
}

function addChecklist() {

  const checklistname = document.querySelector('#title').value;
  fetch(`https://api.trello.com/1/checklists?idCard=${cardID}&name=${checklistname}&key=${key}&token=${token}`, {
    method: 'POST'
  })
    .then(data => data.json())
    .then(jsondata => {
      // console.log(jsondata);
      const addElement = document.querySelector('.checklist-form');
      const div = document.createElement('div');
      div.className = 'checklist';
      div.id = jsondata.id;
      const btn = document.createElement('button');
      const additem = document.createElement('button');
      btn.addEventListener('click', function () {
        deleteChecklist(jsondata.id);
        // console.log(jsondata.id);
      })
      additem.addEventListener('click', function () {
        addItem(jsondata.id);
        // console.log(jsondata.id);
      })
      const p = document.createElement('p');

      div.appendChild(p).innerHTML = checklistname;
      div.appendChild(additem).innerHTML = 'Add item';
      div.appendChild(btn).innerHTML = 'delete';

      addElement.appendChild(div);
    })
  // console.log(addElement.appendChild(document.createElement('div')).innerHTML=checklistname);
}


function deleteChecklist(id) {
  const element = document.getElementById(id);
  fetch(`https://api.trello.com/1/checklists/${id}?key=a8814bee6d62eac3c380bfd0a8fd8d14&token=c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e`, {
    method: 'delete'
  })
  const items = element.nextSibling;
  items.remove();
  element.remove();
}

function addItemAfter(value, myNode, checklistID) {
  while (myNode.firstChild) {
    myNode.removeChild(myNode.firstChild);
  }
  const dtl = document.createElement('input');
  dtl.setAttribute('type', 'button');
  dtl.setAttribute('class', 'checklist-item-dtl');
  dtl.setAttribute('value', 'cancel');


  const btn = document.createElement('input');
  btn.setAttribute('type', 'checkbox');
  btn.setAttribute('class', 'checklist-item-btn')
  btn.setAttribute('value', 'incomplete');

  const small = document.createElement('small');
  small.setAttribute('class', 'checklist-item-input');
  small.innerHTML = value;

  const TempDiv = document.createElement('div');
  TempDiv.setAttribute('class', 'tempDIV');
  TempDiv.appendChild(small);
  TempDiv.appendChild(btn);
  TempDiv.appendChild(dtl);
  // console.log(myNode);
  if (myNode.parentNode.nextSibling === null) {
    const divBaby = document.createElement('div')
    divBaby.setAttribute('class', 'checklist-item');
    divBaby.setAttribute('id', checklistID);
    myNode.parentNode.parentNode.appendChild(divBaby);
  }
  myNode.parentNode.nextSibling.appendChild(TempDiv);

  fetch(`https://api.trello.com/1/checklists/${checklistID}/checkItems?name=${small.innerHTML}&pos=bottom&checked=false&key=a8814bee6d62eac3c380bfd0a8fd8d14&token=c070420f041823349feab1067b6f28036b9a174aa8b42a097de57123538df15e`, {
    method: 'POST',
  })
    .then(data => data.json())
    .then(jdata => {
      // console.log(jdata)
      TempDiv.setAttribute('id', jdata['id'])
      dtl.addEventListener('click', function () {
        removeChecklistItem(checklistID, jdata['id']);
      })
      btn.addEventListener('click', function () {
        checkrUncheck(checklistID, jdata['id']);
      })
      if (jdata['state'] == 'complete') {
        btn.setAttribute('checked', 'false');
      }
    })
  myNode.remove();
}

function removeChecklistItem(checklistID, itemID) {

  // console.log(element.innerHTML);
  // console.log(checklistID);
  // console.log(itemID);
  fetch(`https://api.trello.com/1/checklists/${checklistID}/checkItems/${itemID}?key=${key}&token=${token}`, {
    method: 'DELETE',
  })
  // const element=document.getElementById(checklistID);
  const item = document.getElementById(itemID);
  item.remove();
}

function addItem(checklistID) {
  const newDiv = document.createElement('div');
  newDiv.className = 'checklist-item';

  const input = document.createElement('input');

  input.setAttribute('type', 'text');
  input.setAttribute('class', 'checklist-item-input');
  input.setAttribute('value', 'Add an item');

  const btn = document.createElement('input');
  btn.setAttribute('type', 'button');
  btn.setAttribute('class', 'checklist-item-btn')
  btn.setAttribute('value', 'Add');

  newDiv.appendChild(input);
  newDiv.appendChild(btn);
  const element = document.getElementById(checklistID);
  element.appendChild(newDiv);

  btn.addEventListener('click', () => addItemAfter(input.value, newDiv, checklistID));
}